//Write a program to add and multiply two given matrixes.
#include<stdio.h>
#define MAXsize 100
void add_matrix(int row,int col,int mat1[MAXsize][MAXsize],int mat2[MAXsize][MAXsize]);
int x,y;
int main()
{
    int col,row,mat1[MAXsize][MAXsize],mat2[MAXsize][MAXsize];
    printf("Enter the number of columns:");
    scanf("%d",&col);
    printf("Enter the number of rows:");
    scanf("%d",&row);
    printf("Enter first matrix: \n");
    for(x=0;x<row;x++)
    {
        for(y=0;y<col;y++)
        {
          scanf("%d",&mat1[x][y]);
        }
    }
    printf("Enter second matrix: \n");
    for(x=0;x<row;x++)
    {
        for(y=0;y<col;y++)
        {
          scanf("%d",&mat2[x][y]);
        }
    }
    printf("After adding first matrix and second matrix the answer is\n");
    add_matrix(row,col,mat1,mat2);
}
void add_matrix(int row,int col,int mat1[MAXsize][MAXsize],int mat2[MAXsize][MAXsize])
{
    for(x=0;x<row;x++)
    {
        for(y=0;y<col;y++)
        {
          printf("%d\t",(mat1[x][y]+mat2[x][y]));
        }
    printf("\n");
    }
}
